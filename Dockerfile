FROM python

RUN pip install luigi docker
RUN pip install flask flask-sqlalchemy psycopg2 flask-migrate flask-script marshmallow flask-bcrypt pyjwt
RUN pip install flask_restplus
RUN pip install flask_accepts

RUN pip install -U spacy

RUN python3 -m spacy download es_core_news_md

WORKDIR /usr/src/app