# /run.py
import os

from src import app, env_name

if __name__ == '__main__':
  app.run(host='0.0.0.0',debug=(env_name == 'development'))