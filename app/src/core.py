import spacy
from spacy import displacy

nlp = spacy.load("es_core_news_md", disable=["tagger", "parser"])

def getNerResult(text):
    doc = nlp(text)
    return displacy.render(doc, style="ent")