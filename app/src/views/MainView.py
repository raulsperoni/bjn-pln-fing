# /src/views/UserView

from flask import request, g, render_template
from flask_accepts import accepts, responds
from flask_restplus import Namespace, Resource


from ..views import custom_response

api = Namespace('api', description='API')



@api.route("/")
class API(Resource):

    def post(self):
        """
        Create User
        """


        return custom_response({}, 201)


    def get(self):
        """
        Get users
        """

        return custom_response({}, 200)


