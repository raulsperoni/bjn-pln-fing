import os
from . import core
from flask import Flask, render_template, json, Response, request


from .config import app_config
from .views.MainView import api as main_api
env_name = os.getenv('FLASK_ENV')

app = Flask('fing')
app.config.from_object(app_config[env_name])


@app.route("/")
def get():
    return render_template("index.html")


@app.route('/process', methods=['POST'])
def my_form_post():
    try:
        text = request.form['text']
        result = core.getNerResult(text)
        return custom_response(result, 200)
    except Exception as e:
        print(e)
        return custom_response(str(e), 400)


def custom_response(res, status_code):
    """
    Custom Response Function
    """
    return Response(
        mimetype="application/json",
        response=json.dumps(res),
        status=status_code
    )


